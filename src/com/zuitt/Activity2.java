package com.zuitt;

import java.util.ArrayList;
import java.util.HashMap;

public class Activity2 {

    public static void main(String[] args){

//      declare an array and assign the first 5 prime numbers
        int[] firstFivePrimeArray = new int[5];

        firstFivePrimeArray[0] = 2;

        System.out.println("The first prime number is: " + firstFivePrimeArray);

        ArrayList<String> friends = new ArrayList<String>();

        friends.add("John");
        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> inventory = new HashMap<>();

        inventory.put("toothbrush", 20);
    }
}
